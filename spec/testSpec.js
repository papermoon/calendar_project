var request = require("request");

describe("Hello World Server", function () {
    describe("GET /", function () {

        it("returns status code 200", function (done) {
            request.get("http://localhost:4000", function (error, response, body) {
                expect(response.statusCode).toBe(200);
                done();
            });
        });

        it("returns array", function (done) {
            request.get("http://localhost:4000/eventsByDay/2018-0", function (error, response, body) {
                console.log(body);
                
                
                expect(body.length).toBe(2);
                done();
            });
        });
    });
});