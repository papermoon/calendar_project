const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const jwt = require('jsonwebtoken');
const crypt = require('bcrypt');
const uuid =require('uuid/v4');

const Event = require('./models/event');
const User = require("./models/user");

const router = express.Router;
const app = express();

app.use(express.static('public'));
app.use(morgan('dev'));
app.use(helmet());
app.use(bodyParser.json({extended: true}));
app.use(bodyParser.urlencoded({extended: true}));


/* MONGOOSE CONNECTION */
mongoose.connect('mongodb://admin-pai:sqay4Um7AiU7@ds213688.mlab.com:13688/event-callendar')
    .then(() => console.log('Database connected!!'))
    .catch(err => console.error(err));

app.set('view engine', 'ejs');


app.get('/', function(req, res) {
    const token = req.header("Autorization");

    res.redirect('/login');
});


app.get('/daily_events/:date', function(req, res){
    const currentDate = req.params.date;
    // get date for events 

    res.render('daily_events', {
        dates: [ 1, 2, 3, 4, 5, 6 ],
    });
});


app.get("/eventsByDay/:date", function(req, res) {
    const currentDate = req.params.date;
    const auth = req.header('autorization');
    let date = new Date(...currentDate);

    const token = jwt.verify(auth,'SESSION SECRET!!!!!')

    if (!token) {
        return res.status(401).send();
    }

    Event.find({
        userKey: token.key,
        date: currentDate
    }).then(data => {
        res.json(data);
    });
});

app.get("/eventsByMonth/:date", async function(req, res) {
    const currentDate = req.params.date;
    const auth = req.header('autorization');

    const token = jwt.verify(auth,'SESSION SECRET!!!!!');
    let regexp = new RegExp("^"+ currentDate);
    let data =  await Event.find({userKey: token.key, date: regexp});
    let dataaaaa = data.map(d => d.date.split('-').pop());
    res.status(200).send(dataaaaa);

});


app.post('/newEvent', function(req, res) {
    const date = req.body.date;

    const auth = req.header('autorization');
    const token = jwt.verify(auth,'SESSION SECRET!!!!!')

    if (!token) {
        return res.status(401).send();
    }

    new Event({
      tittle: req.body.tittle,
      startTime: req.body.startTime,
      date: date,
      userKey: token.key,
    }).save();

    Event.find({userKey: token.key}).then(res => {console.log(res)});

    res.json({ok: 1});
});

app.get('/calendar', function (req, res) {
    res.redirect('calendar/2018-1');
});

app.get('/calendar/:date', function (req, res) {
    res.render('calendar');
});


app.get('/admin', function (req, res) {
    res.render('admin');
});

app.get('/users', async function (req, res) {

    const auth = req.headers('Autorization');

    if (!auth) {
        return res.status(401).send('No auth token');
    }

    const users = await User.find({});


    res.status(200).send(users);
});

app.get('/login', function (req, res){
    res.render('login');
});

app.get('/register', function (req, res) {
    res.render('register');
});


app.post('/login', async function (req, res){

    const password = req.body.password;
    const email = req.body.email;

    const user = await User.findOne({email: email});

    if (!user) {
        return res.status(400).send({error: 'No user'});
    }


    if (!crypt.compareSync(password, user.hash)) {
        return res.status(401).send({error: `wrong password for ${user.email}`});
    }

    const tokenBody = {
        email: user.email,
        key: user.userKey,
        role: user.role,
    }

    const token = jwt.sign(tokenBody, 'SESSION SECRET!!!!!', {expiresIn: 50000});

    return res.status(200).send({token: token});
});

app.post('/register', async function (req, res) {

    const password = req.body.password;
    const email = req.body.email;

    const user = await User.findOne({email: email});
    
    if (user) {
        return res.status(400).send({error: 'user alredy exist'});
    }
    
    const hash = crypt.hashSync(password, 10);
    
    const newUser = {
        email: email,
        hash: hash,
        userKey: uuid(),
        role: 'user'
    }
    
    User(newUser).save((err, data) => {
        if (err) {
            return res.status(501).send({error: 'internal error while saving user'});
        }

        const tokenBody = {
            email: data.email,
            key: data.userKey,
            role: 'user',
        }

        const token = jwt.sign(tokenBody, 'SESSION SECRET!!!!!', {expiresIn: 50000});

        return res.status(200).send({token: token});
    })
});

app.post('/isValid', async function (req, res) {
    const token = req.body.token;

    let ret = jwt.verify(token, 'SESSION SECRET!!!!!');

    if (ret) {
        res.status(200).send({email: ret.email})
    }
});


app.listen(4000, function(){
    console.log('App running on http://localhost:4000')
});
