function logout() {
  window.localStorage.setItem('authToken', '');
}


function getData() {

  const token = window.localStorage.getItem('authToken');

  if (!token) {
    window.location.href = '/login';
  }

  $.ajax({
    url: "http://localhost:4000/users",
    type: "get",
    headers: {
      Autorization: token,
    },
    dataType: "json",
    success: data => {
      console.log(data);
    },
    error: (status, text) => {
      window.location.href = '/login';
    }
  });
}

$(document).ready(() => {
  getData();
});