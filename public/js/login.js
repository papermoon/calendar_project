
/* event handler for registration */
$("#reg").submit(event => {
    event.preventDefault();

    let email = $("#regEmail").val();
    let password = $("#regPass").val();

    $.ajax({
        url: "http://localhost:4000/register",
        type: "post",
        data: {
            email: email,
            password: password,
        },
        dataType: "json",
        success: data => {
            window.localStorage.setItem('authToken', data.token);
            window.location.href="/calendar/2018-1"
        },
        error: (status ,text) => {
            console.log(status, text);
        }
      });
})


/* event handler for registration */
$("#log").submit(event => {
    event.preventDefault();

    let email = $("#exampleInputEmail1").val();
    let password = $("#exampleInputPassword1").val();

    $.ajax({
        url: "http://localhost:4000/login",
        type: "post",
        data: {
            email: email,
            password: password,
        },
        dataType: "json",
        success: data => {
            window.localStorage.setItem('authToken', data.token);
            console.log(data)
            if (data.userType === 'admin') { 
                window.location.href="/admin"
            } else {
                window.location.href="/calendar/2018-1"
            }
        },
        error: (status ,text) => {
            console.log(status, text);
        }
      });
})