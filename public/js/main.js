

function daysInMonth(month,year) {
  return new Date(year, month, 0).getDate();
}

async function updateCallendar() {
  let urlTime =  window.location.href.split('/').pop();
  let currentDate = new Date(...urlTime.split('-'));

  console.log(currentDate);

  let currentDay = currentDate.getDate(),
      currentYear = currentDate.getFullYear(),
      currentMonth = currentDate.getMonth();
  
  let dayNow = new Date().getDate(),
      monthNow = new Date().getMonth(),
      yearNow = new Date().getFullYear();

  console.log(dayNow, monthNow, yearNow)

  const nDays = new Date(currentYear, currentMonth + 1, 0).getDate();
  const startDay = new Date(currentYear, currentMonth, 1).getDay();

  /* 
  days of the week
  1 -> 0
  2 -> 1
  3 -> 2
  4 -> 3
  5 -> 4
  6 -> 5
  0 -> 6
  */

  const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  $("#currentMonth").text(monthNames[currentMonth]);
  $("#currentYear").text(currentYear);

  if (startDay) {
    for (let i = 0; i < startDay - 1; i++) {
      $("#callendar").append(`<li> </li>`);
    }
  } else {
    for (let i = 0; i < 6; i++) {
      $("#callendar").append(`<li> </li>`);
    }
  }
  
  // we need to take all children with data from api
  let daysWithEvent = (await eventDays()).map(r => Number(r));
  console.log(daysWithEvent);
  
  for (let i = 0; i < nDays; i++) {
    let time = new Date(currentYear, currentMonth, i + 1)    
    let timeStr = `${time.getFullYear()}-${time.getMonth()}-${time.getDate()}`;
    
    if (i + 1 === dayNow && monthNow === currentMonth && yearNow === currentYear) {
      $("#callendar").append(`<li class="active"><a class="day active" href="/daily_events/${timeStr}">${i + 1}</a></li>`);
    } else if (daysWithEvent.indexOf(i + 1) >= 0) {
      $("#callendar").append(`<li class="active2"><a class="day active2" href="/daily_events/${timeStr}">${i + 1}</a></li>`);
    } else {
      $("#callendar").append(`<li><a class="day" href="/daily_events/${timeStr}">${i + 1}</a></li>`);
    }
  }
}


function eventDays() {
  return new Promise((res, rej) => {
    let token = window.localStorage.getItem('authToken');
    const dateTime = window.location.href.split("/").pop();

    if (!token) {
      window.location.href = '/login';
    }
    $.ajax({
      url: "http://localhost:4000/eventsByMonth/" + dateTime,
      type: "get",
      headers: {
        Autorization: token,
      },
      dataType: "json",
      success: data => {
        console.log('OK');
        res(data);
        console.log(data);
      },
      error: (status ,text) => {
        rej('');
          console.log(status, text);
      }
    });
  })
}


function nextMonth() {
  let currentDate = window.location.href.split("/").pop();
  currentDate = new Date(...currentDate.split('-'));
  currentDate.setMonth(currentDate.getMonth() + 1);
  let timeStr = `${currentDate.getFullYear()}-${currentDate.getMonth()}`;
  window.location.href = `/calendar/${timeStr}`;
}

function prevMonth() {
  let currentDate = window.location.href.split("/").pop();
  currentDate = new Date(...currentDate.split("-"));
  currentDate.setMonth(currentDate.getMonth() - 1);
  let timeStr = `${currentDate.getFullYear()}-${currentDate.getMonth()}`;
  window.location.href = `/calendar/${timeStr}`;
}

function getCredentials() {
  let token = window.localStorage.getItem('authToken');

  if (!token) {
    window.location.href = '/login';
  }
  $.ajax({
    url: "http://localhost:4000/isValid",
    type: "post",
    data: {
      token: token,
    },
    dataType: "json",
    success: data => {
      console.log('OK');
      console.log(data)
      $("#userEmail").text(data.email);
    },
    error: (status ,text) => {
        console.log(status, text);
    }
  });
}

function logout() {
  window.localStorage.setItem('authToken', '');
}


$(document).ready(() => {
  getCredentials();
  updateCallendar();
});