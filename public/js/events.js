

function setTime() {
  let currentDate = window.location.href.split("/").pop();
  currentDate = new Date(...currentDate.split("-"));

  const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
  
  const dateStr = `${currentDate.getDate()}. ${monthNames[currentDate.getMonth()]}`;

  $("#currentDate").text(dateStr);
  $("#currentYear").text(currentDate.getUTCFullYear());
}


function submit() {

  let eventTitle = $('#title').val(),
      eventStartTime = $('#start').val();

  const dateTime = window.location.href.split("/").pop()

  if (eventTitle.length === 0) {
    console.log('error');
    return;
  }

  if (!/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/.test(eventStartTime)) {
    console.log('error')
    return;
  }

  let token = window.localStorage.getItem('authToken');

  $.ajax({
    url: "http://localhost:4000/newEvent",
    type: "post",
    data: {
      tittle: eventTitle,
      startTime: eventStartTime,
      date: dateTime,
    },
    headers: {
      Autorization: token,
    },
    dataType: "json",
    success: data => {
      // updateEvents();
            window.location.reload();
      console.info(JSON.stringify(data));
    }
  });

}

function updateEvents() {
  const dateTime = window.location.href.split("/").pop();
  let token = window.localStorage.getItem('authToken');
  console.log(token)
  $.ajax({
    url: "http://localhost:4000/eventsByDay/" + dateTime,
    type: "get",
    headers: {
      Autorization: token,
    },
    dataType: "json",
    success: data => {
      createTabel(data); 
    }
  });
}


function createTabel(data) {

  for (let i = 0; i < data.length; i++) {
    const element = data[i];
    $('#tabele').append(
      `<tr><td>${element.startTime}</td><td>${element.tittle}</td><td><button id="${element._id}" onclick="remove()"> Delete </button></td></tr>`
    )
  }
}

function remove(){
  console.log(id);
  
}



function getCredentials() {
  let token = window.localStorage.getItem('authToken');
  if (!token) {
    window.location.href = '/login';
  }
  $.ajax({
    url: "http://localhost:4000/isValid",
    type: "post",
    data: {
      token: token,
    },
    dataType: "json",
    success: data => {
      console.log('OK');
      console.log(data)
      $("#userEmail").text(data.email);
    },
    error: (status ,text) => {
        console.log(status, text);
    }
  });
}

function logout() {
  window.localStorage.setItem('authToken', '');
}


$(document).ready(() => {
  getCredentials();
  setTime();
  updateEvents();
});
